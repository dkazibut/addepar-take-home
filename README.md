# Addepar Take Home

`npm install` to install dependencies

`npm run start` to start the dev server and see the assignment locally

`npm run build` to build a distributable folder 

## Fixes and wishlist

* The API key should be in an .env file and referenced that way
* I'd use common.js or ES modules to separate the code. Right now everything is too linked to this 'app'
* Error checking is 'dumb' and the messages aren't helpful to the user
* there's no sanitation of the input on the form. All it's checking is max length
* isMarketOpen() is only checking from 9 a.m. - 4 p.m., not 4:30 p.m.
* isMarketOpen() should be written using moment.js
* findstock() need to be split up into more functions. It's too hard to read
* findstock() should be rewritten to use async/await probably

This was a lot of fun! Thanks for the challenge.

David