const _stockTracker = {
  config: {
    apiKey: '5LR78U420BL0COM7' // This is really dumb, but I didn't have time to create a .env file
  },
  isMarketOpen() {
    /**
     * Hours of opertation:
     * M - F
     * 
     * 9:30 a.m. - 4:00 p.m.
     * 
     */
    
    let isWeekday              = false;
    let isDuringOperatingHours = false;
    let isOpen                 = false;
    let label                  = document.getElementById('textIsMarketOpen')
    let date                   = new Date();
    let currentHour            = date.getHours();
    let currentMinutes         = date.getMinutes();
    let currentDay             = date.getDay();

    isWeekday = ([0,6].includes(currentDay)) ? false : true;
    isDuringOperatingHours = (currentHour >= 9 && currentHour <= 16) ? true : false;

    isOpen = (isWeekday === true && isDuringOperatingHours === true) ? "open": "closed";

    label.innerHTML = isOpen;

  },
  lookUpStock() {
    /**
     * 1.) hit enter and lookup stock
     * 2.) present list of matches
     *       - error message if there are none
     * 
     */

    let form = document.getElementById('tickerSearch');
    
    let findStock =  function(e) {
      // https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=BA&apikey=demo
      // TODO: Scrub the stockSymbol field for invalid characters

      e.preventDefault();
      
      let query  = document.getElementById('stockSymbol').value;
      let label  = document.getElementById('tickerResults');

      if (query.length > 0) {
        let url      = `https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=${query}&apikey=${_stockTracker.config.apiKey}`
        let response = fetch(url);

        label.innerHTML = '';

        response
        .then(response => response.json())
        .then(function(matches) {

          matches = matches.bestMatches;

          let list = '';

          for (let i = 0;i < matches.length; i++) {
            let symbol   = matches[i]['1. symbol'];
            let name     = matches[i]['2. name'];
            let listItem = `<a href="https://www.alphavantage.co/query?function=TIME_SERIES_WEEKLY&symbol=${symbol}&apikey=${_stockTracker.config.apiKey}" target="_blank">${name} (${symbol})</a><br>`
            
            list = list + listItem;

          }

          label.innerHTML = list;
          
        })
        .catch(label.innerHTML = `<span style='color:red'>Oops, there was an error</span>`)
      } else {
        label.innerHTML = `<span style='color:red'>please enter a symbol</span>`
      }
      
    }

    form.addEventListener('submit', findStock, false);

  },
  init() {
    _stockTracker.isMarketOpen();
    _stockTracker.lookUpStock();
  }
}

window.onload = _stockTracker.init;